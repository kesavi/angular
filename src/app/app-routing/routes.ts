import {Routes} from '@angular/Router';
import { MenuComponent } from '../menu/menu.component';
import { DishDetailComponent } from '../dish-detail/dish-detail.component';
import { HomeComponent } from '../home/home.component';
import { AboutComponent } from '../about/about.component';
import { ContactComponent } from '../contact/contact.component';

export const ROUTES:Routes= [{
	path:'',
	redirectTo: '/home',
	pathMatch: 'full'
},{
	path:'home',
	component: HomeComponent
},{
	path:'menu',
	component: MenuComponent
},{
	path:'about',
	component: AboutComponent
},{
	path:'contact',
	component: ContactComponent
},{
	path:'dishdetail/:id',
	component: DishDetailComponent
}];
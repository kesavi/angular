import { Component, OnInit, ModuleWithProviders,Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {Dish} from '../shared/dish';
import {Comment} from '../shared/comment';
import {DishService} from '../services/dish.service';
import { visibility, flyInOut,expand } from '../animations/app.animation';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-dish-detail',
  templateUrl: './dish-detail.component.html',
  styleUrls: ['./dish-detail.component.scss'],
  host: {
  '[@flyInOut]': 'true',
  'style': 'display: block;'
  },
  animations: [
     visibility(),
     flyInOut(),
     expand()
  ]
})

export class DishDetailComponent implements OnInit {

  dish: Dish;
  dishIds: number[];
  prev: number;
  next: number;
  errMsg: string;
  dishcopy = null;
  visibility = 'shown';

  commentForm: FormGroup;
  comment: Comment;

  formErrors= {
    'author': '',
    'rating': '',
    'comment': ''
  };
  validationMessages= {
    'author' : {
      'required': 'Author name is required',
      'minlength': 'first name must be at least 2 characters long'
    },
    'comment' : {
      'required': 'comment is required'
    }
  };

  constructor(private dishservice: DishService,
  	private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL) { 
    this.createForm();
  }

  ngOnInit() {
  	this.dishservice.getDishIds().subscribe(dishIds => this.dishIds= dishIds);
    this.route.params.switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
          errmess => { this.dish = null; this.errMsg = <any>errmess; });
  }

  setPrevNext(dishId: number): void{
  	let index= this.dishIds.indexOf(dishId);
  	this.prev= this.dishIds[(this.dishIds.length + index -1)%this.dishIds.length];
  	this.next= this.dishIds[(this.dishIds.length + index +1)%this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  createForm(){
      this.commentForm = this.fb.group({
      author: ['',[Validators.required,Validators.minLength(2)]],
      rating: 5,
      comment: ['',  [Validators.required]],
      date: ''
    });
    this.commentForm.valueChanges.subscribe(data => this.onValueChanged());
    this.onValueChanged();
  }

  onValueChanged(data?:any){
    if(!this.commentForm){
      return;
    }
    const form= this.commentForm;
    for(const field in this.formErrors){
      this.formErrors[field] = '';
      const control = form.get(field);
      if(control && control.dirty && !control.valid){
        const messages = this.validationMessages[field];
        for(const key in control.errors){
          this.formErrors[field] +=messages[key] + '';
        }
      }
    }
  }

  onSubmit(){
    this.comment= this.commentForm.value;
    this.comment.date= new Date().toISOString();
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save()
      .subscribe(dish => { this.dish = dish; console.log(this.dish); });
    this.commentForm.reset({
      author: '',
      rating: 5,
      comment: ''
    });
  }
}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Feedback, CONTACTTYPE}  from '../shared/feedback';
import { flyInOut,expand } from '../animations/app.animation';
import {FeedbackService} from '../services/feedback.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
    host: {
  '[@flyInOut]': 'true',
  'style': 'display: block;'
  },
  animations: [
     flyInOut(),
     expand()
  ]
})
export class ContactComponent implements OnInit {
  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType= CONTACTTYPE;
  resFeedback= null;
  processing= false;
  errMsg= '';
  formErrors= {
  	'firstName': '',
  	'lastName': '',
  	'telNum': '',
  	'email': ''
  };
  validationMessages= {
  	'firstName' : {
  		'required': 'first name is required',
  		'minlength': 'first name must be at least 2 characters long',
  		'maxlength': 'first name can not be more than 20 characters long'
  	},
  	'lastName' : {
  		'required': 'last name is required',
  		'minlength': 'last name must be at least 2 characters long',
  		'maxlength': 'last name can not be more than 20 characters long'
  	},
  	'telNum' : {
  		'required': 'Telephone number is required',
  		'pattern': 'Tel number must contain only numbers'
  	},
  	'email' : {
  		'required': 'Email is required',
  		'email': 'email not in valid format'
  	}
  };
  constructor(private fb:FormBuilder,
    private feedbackService:FeedbackService) {
  	this.createForm();
  }

  ngOnInit() {}

  createForm(){
  	this.feedbackForm = this.fb.group({
  		firstName: ['',[Validators.required,Validators.minLength(2),Validators.maxLength(20)]],
  		lastName: ['',[Validators.required,Validators.minLength(2),Validators.maxLength(20)]],
  		telNum: [0,	[Validators.pattern,Validators.required]],
  		email:['', [Validators.required,Validators.email]],
  		agree: false,
  		contactType: 'none',
  		message: ''
  	});
  	this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged());
  	this.onValueChanged();
  }

  onValueChanged(data?:any){
  	if(!this.feedbackForm){
  		return;
  	}
  	const form= this.feedbackForm;
  	for(const field in this.formErrors){
  		this.formErrors[field] = '';
  		const control = form.get(field);
  		if(control && control.dirty && !control.valid){
  			const messages = this.validationMessages[field];
  			for(const key in control.errors){
  				this.formErrors[field] +=messages[key] + '';
  			}
  		}
  	}
  }

  onSubmit(){
  	this.feedback= this.feedbackForm.value;
    this.processing=true;
    this.feedbackService.submitFeedback(this.feedback)
    .subscribe(feedback => this.showSubmittedFeedback(feedback),
      errmess => this.onFailure(errmess));
  	this.feedbackForm.reset({
  		firstName: '',
  		lastName: '',
  		telNum: 0	,
  		email:'',
  		agree: false,
  		contactType: 'none',
  		message: ''
  	});
  }
  showSubmittedFeedback(feedback:Feedback){
    this.resFeedback= feedback;    
    this.processing= false;
    setTimeout(()=>{this.resFeedback=null;},5000);
  }
  onFailure(errmess){
    this.resFeedback = null;
    this.processing= false; 
    this.errMsg = <any>errmess; 
    setTimeout(()=>{this.errMsg='';},5000);
  }
}

export class  Feedback{
	firstName: string;
	lastName: string;
	telNum: number;
	email: string;
	agree: boolean;
	contactType: string;
	message: string;
};

export const CONTACTTYPE= ['None','Tel','Email'];